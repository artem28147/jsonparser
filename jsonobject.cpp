#include "jsonobject.h"

void JSONObject::parse(std::string& json, int &pos)
  {
      int length=json.length();
      while(pos<length && json.at(pos)!='}')
      {
          pos++;
          std::string key;
          if(json.at(pos)=='\"' || json.at(pos)=='\'')
          {
              JSONString temp;
              temp.parse(json,pos);
              key=temp.toString();
              pos++;
          }
          else
          {
              int newpos=json.find(':',pos);
              std::string key=json.substr(pos,newpos-pos-1);
              pos=newpos+1;
          }
          JSONType* value=MyParser::parseString(json,pos);
          this->append(key,value);
      }
      pos++;
  }

JSONObject::~JSONObject()
{
	for (std::map<std::string, JSONType*>::iterator it = fields.begin(); it != fields.end(); it++)
	{
		delete it->second;
	}
	fields.clear();
}

JSONType* JSONObject::get(char* field)
{
	JSONType* result;
	std::map<std::string, JSONType*>::iterator it = fields.find(std::string(field));
	if (it != fields.end())
	{
		result = it->second;
	}
	else
	{
		result = 0;
	}
	return result;
}


std::string JSONObject::toString()
{
	std::string result = "{";
	std::vector<std::string> keys;
	for (std::map<std::string, JSONType*>::iterator it = fields.begin(); it != fields.end(); it++)
	{
		keys.push_back(it->first);
	}
	for (std::vector<std::string>::iterator it = keys.begin(); it != keys.end(); it++)
	{
		result += *it + " : " + fields[*it]->toString() + ",";
	}
	result += "}";
	return result;
}

JSONObject* JSONObject::append(std::string field, JSONType* value)
{
	fields.insert(std::pair<std::string, JSONType*>(field, value));
	return this;
}
