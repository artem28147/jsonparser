#pragma once
#include "jsontype.h"

class JSONValue : public JSONType
{
private:
    std::string value;
public:
	JSONValue(){}
	~JSONValue(){}

	JSONValue(std::string& value);

	void parse(std::string& json, int &pos);

	std::string toString();
};
