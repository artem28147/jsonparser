#include "jsonstring.h"

JSONString::JSONString(std::string& str)
{
	value = str;
}

std::string JSONString::toString()
{
	return value;
}
void JSONString::parse(std::string& json, int &pos)
{
	std::string result;
	pos++;
	char currentChar = json.at(pos);
	while (currentChar != '\"')
	{
		if (currentChar == '\\')
		{
			pos++;
			currentChar = json.at(pos);
			switch (currentChar)
			{
			case 'b':
			{
						result.append(1, '\b');
						break;
			}
			case 'f':
			{
						result.append(1, '\f');
						break;
			}
			case 'n':
			{
						result.append(1, '\n');
						break;
			}
			case 'r':
			{
						result.append(1, '\r');
						break;
			}
			case 't':
			{
						result.append(1, '\t');
						break;
			}
			case 'u':
			{
						char ch = strtol(json.substr(pos + 1, 4).c_str(), 0, 16);
						result.append(1, ch);
						pos += 4;
						break;
			}
			default:
			{
					   result.append(1, currentChar); }
			}
		}
		else
		{
			result.append(1, currentChar);
		}
		pos++;
		currentChar = json.at(pos);
	}
	pos++;
	this->value = result;
}
std::string JSONString::getValue()
{
	return value;
}