#pragma once
#include "jsontype.h"
#include <iostream>
#include <sstream>

class JSONNumber : public JSONType
{
private:
    double value;
public:
	JSONNumber(){}
	~JSONNumber(){}

	JSONNumber(std::string &str);

	void parse(std::string &json, int &pos);

	std::string toString();

	double getValue();
};

