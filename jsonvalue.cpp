#include "jsonvalue.h"

JSONValue::JSONValue(std::string& value)
{
	this->value = value;
}

void JSONValue::parse(std::string& json, int &pos)
{
	std::string result;
	char currentChar = json.at(pos);
	if (currentChar == '\"' || currentChar == '\'')
	{
		pos++;
		result.append(json.substr(pos, json.find(currentChar, pos + 1) - pos));
		pos = json.find(currentChar, pos + 1) + 1;
	}
	else
	{
		while (currentChar != ']' && currentChar != '}' && currentChar != ',')
		{
			result.append(1, currentChar);
			pos++;
			currentChar = json.at(pos);
		}
	}
	this->value = (result);
}


std::string JSONValue::toString()
{
	return value;
}