#pragma once
#include "jsontype.h"

class JSONBool : public JSONType
{
private:
    bool value;
public:
	JSONBool(){}
	~JSONBool(){}

	void parse(std::string& json, int &pos);

	bool getValue();

	std::string toString();
};
