#include "jsonarray.h"


JSONArray::~JSONArray()
{
	for (auto &it : arrayData) delete it;
	arrayData.clear();
}

void JSONArray::parse(std::string& json, int &pos)
{
    while(json.at(pos)!=']')
    {
        pos++;
        JSONType *value=MyParser::parseString(json, pos);
        this->append(value);
    }
    pos++;
}

JSONArray* JSONArray::append(JSONType *element)
{
	arrayData.push_back(element);
	return this;
}

JSONType* JSONArray::get(int index)
{
	return arrayData.at(index);
}

std::string JSONArray::toString()
{
	std::string result = "[";
	for (unsigned int i = 0; i<arrayData.size(); i++)
	{
		result += arrayData.at(i)->toString() + ",";
	}
	result += "]";
	return result;
}
