#pragma once
#include "jsontype.h"

class JSONNull : public JSONType
{
public:
	JSONNull(){}
	~JSONNull(){}

	void parse(std::string& json, int &pos);

	std::string toString();
};
