#pragma once
#include "jsontype.h"
#include "jsonarray.h"
#include "jsonobject.h"
#include "JSONValue.h"
#include "jsonstring.h"
#include "jsonnumber.h"
#include "jsonbool.h"
#include "jsonnull.h"
#include <locale>
#include <vector>

class MyParser
{
private:
    static int pos;

	static std::string simplify(std::string & str);


public:
	static JSONType* parseString(std::string & json, int &pos);

    MyParser(){}
    ~MyParser(){}

	static JSONType* parse(std::string & json);
};

