#pragma once
#include <string>

class JSONType
{
public:
	JSONType(){}
	~JSONType(){}

    virtual void parse(std::string& json, int &pos) = 0;

    virtual std::string toString() = 0;
};

