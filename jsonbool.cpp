#include "jsonbool.h"

void JSONBool::parse(std::string& json, int &pos)
{
	if (json.at(pos) == 't')
	{
		this->value = true;
		pos += 4;
	}
	else
	{
		this->value = false;
		pos += 5;
	}
}

bool JSONBool::getValue()
{
	return this->value;
}

std::string JSONBool::toString()
{
	if (this->value == true)
		return std::string("true");
	else
		return std::string("false");
}