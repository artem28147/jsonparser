#include "jsonnumber.h"

JSONNumber::JSONNumber(std::string &str)
{
	if (str.find('E') != std::string::npos || str.find('e') != std::string::npos)
	{
		std::istringstream stream(str);
		stream >> value;
	}
	else
	{
		this->value = ::atof(str.c_str());
	}
}

void JSONNumber::parse(std::string &json, int &pos)
{
	std::string result;
	char currentChar = json.at(pos);
	while (currentChar != ']' && currentChar != '}' && currentChar != ',')
	{
		result.append(1, currentChar);
		pos++;
		currentChar = json.at(pos);
	}
	if (result.find('E') != std::string::npos || result.find('e') != std::string::npos)
	{
		std::istringstream stream(result);
		stream >> value;
	}
	else
	{
		this->value = ::atof(result.c_str());
	}
}

double JSONNumber::getValue()
{
	return this->value;
}

std::string JSONNumber::toString()
{
	std::ostringstream stream;
	stream << value;
	return std::string(stream.str());
}
