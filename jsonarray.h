#pragma once
#include "jsontype.h"
#include "myparser.h"
#include <vector>

class JSONArray : public JSONType
{
private:
    std::vector<JSONType*> arrayData;
public:
	JSONArray(){}
	~JSONArray();

    void parse(std::string & json, int & pos);

	JSONArray* append(JSONType *element);

	JSONType* get(int index);

	std::string toString();
};

