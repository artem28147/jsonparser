#pragma once
#include "jsontype.h"
#include "jsonstring.h"
#include "myparser.h"
#include <map>
#include <algorithm>
#include <iterator>
#include <vector>

class JSONObject : public JSONType
{
private:
    std::map<std::string, JSONType*> fields;
public:
	JSONObject(){}
	~JSONObject();

	void parse(std::string& json, int &pos);

	JSONType* get(char* field);

	std::string toString();

	JSONObject* append(std::string field, JSONType* value);
};
