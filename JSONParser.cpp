#include <iostream>
#include <fstream>
#include <streambuf>
#include <stdio.h>
#include <stdlib.h>
#include "time.h"
#include "myparser.h"
#include "windows.h"
int main(int argc, char *argv[])
{
	FILE * pFile;
	long lSize;
	int result;
	clock_t begin;
	clock_t end;

	std::ifstream input;
	input.open("C:\\Users\\Artem\\Downloads\\generated500kb.json", std::ifstream::in);
	
	std::string *str = new std::string(std::istreambuf_iterator<char>(input), std::istreambuf_iterator<char>());

	begin = clock();
	JSONType* obj = MyParser::parse(*str);
	end = clock(); 
	std::string stt;
	std::cout <<  static_cast<JSONObject*>(obj)->toString()<<'\n';
	std::cout << (end - begin) / CLOCKS_PER_SEC << std::endl;
	delete str;
	system("pause");
	return 0;
}
