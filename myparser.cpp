#include "myparser.h"

int MyParser::pos=0;

JSONType* MyParser::parse(std::string & json)
{
	std::string jsonString;
	jsonString = simplify(json);
	JSONType* result = parseString(jsonString, pos);
	return result;
}

std::string MyParser::simplify(std::string & str)
{
	char* buf = new char[str.size()];
	int pos = 0;
	char currentQuotes = 0;
	bool previousScreened = false;
	for (std::string::iterator it = str.begin(); it != str.end(); it++)
	{
		//���� �� ������ �������� " ��� '
		if (*it == '\"' || *it == '\'')
		{
			//�������� ���
			if (currentQuotes == 0)
			{
				currentQuotes = *it;
			}
			else if (*it == currentQuotes && !previousScreened)
			{
				currentQuotes = 0;
			}
		}
		if (std::isspace(*it, std::locale()) && currentQuotes == 0)
		{
		}
		else
		{
			buf[pos++] = *it;
		}
		if (*it == '\\')
		{
			previousScreened = true;
		}
		else
		{
			previousScreened = false;
		}
	}
	std::string result(buf);
	delete[] buf;
	return result;
}

JSONType* MyParser::parseString(std::string & json, int & pos)
{
    JSONType *obj;
    char at=json.at(pos);
	obj = 0;
    if(at=='{')
    {
        obj = new JSONObject();
        obj->parse(json,pos);
    }
    else if(at=='[')
    {
        obj = new JSONArray();
        obj->parse(json,pos);
    }
    else if(at=='\"' ||at=='\'')
    {
        obj = new JSONString();
        obj->parse(json,pos);
    }
    else if((at>='0' && at <='9') || at=='-')
    {
        obj = new JSONNumber();
        obj->parse(json,pos);
    }
    else if(at =='f' || at=='t')
    {
        obj = new JSONBool();
        obj->parse(json,pos);
    }
	else if (at == 'n')
    {
        obj=new JSONNull;
        obj->parse(json,pos);
    }
    return obj;
}


