#pragma once
#include "jsontype.h"

class JSONString : public JSONType
{
private:
    std::string value;
public:
	JSONString(){}
	JSONString(std::string& str);
	~JSONString(){}

	void parse(std::string& json, int &pos);

	std::string getValue();

	std::string toString();
};